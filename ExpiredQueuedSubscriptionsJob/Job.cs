using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using HttpClientBuilder;
using HttpClientBuilder.Extensions;

namespace ExpiredQueuedSubscriptionsJob
{
    public static class Job
    {
       


        [FunctionName("Function1")]
        public static void RunProcessExpiredSubscriptions([TimerTrigger("0 */5 * * * *")]TimerInfo myTimer, ILogger log, ExecutionContext context )
        {
            log.LogInformation($"ProcessExpiredSubscriptions start at: {DateTime.Now}");

            var config = new ConfigurationBuilder()
           .SetBasePath(context.FunctionAppDirectory)
           .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
           .AddEnvironmentVariables()
           .Build();
            var helper = new Helper(config, ExpiredQueuedSubscriptionsJob.Enviroments.TEST, log);
            helper.ProcessExpiredSubscriptions();

        }
    }
}
