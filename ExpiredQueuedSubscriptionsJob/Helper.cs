﻿using HttpClientBuilder;
using IdentityModel.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace ExpiredQueuedSubscriptionsJob
{
    public enum Enviroments
    {
        PROD,
        TEST,
        UAT
    }
    public class Helper
    {
        private IConfigurationRoot _config;
        private Enviroments _environment;
        private ILogger _log;
        public Helper (IConfigurationRoot config, Enviroments environment, ILogger log)
        {
            _config = config;
            _environment = environment;
            _log = log;
        }

        public string GetAuthToken()
        {
            var _tokenClient = new TokenClient(
                _config["TokenUrl" + _environment.ToString()],
                _config["AuthClientId"],
                _config["AuthClientSecret" + _environment.ToString()]);
            var result = _tokenClient.RequestClientCredentialsAsync("rvsPlatformServices").Result;
            var _clientAccessToken = new JwtSecurityToken(result.AccessToken);
            return _clientAccessToken.RawData;
        }

        public void ProcessExpiredSubscriptions ()
        {            
            var authToken = GetAuthToken();
            var httpRequestFactory = new HttpRequestFactory();
            var url = _config["EntitlementsServiceProcessExpiredSubscriptions-"+_environment.ToString()];
            var result = httpRequestFactory.Post(url,null, authToken).Result;
            if (!result.IsSuccessStatusCode)            
                _log.LogWarning($"Unable to process expired subscriptions: " + JsonConvert.SerializeObject(result));         
        }

    }
}
